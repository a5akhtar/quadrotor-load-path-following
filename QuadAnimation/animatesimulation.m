%% Cool simulator
tic
qm_matfilename = 'quad_starmac.mat';    %filename for the mat file for the quadrotor model
load(qm_matfilename);

axis_x_min = min(x)-1;
axis_x_max = max(x)+1;
axis_y_min = min(y)-1;
axis_y_max = max(y)+1;
axis_z_min = min(z)-1;
axis_z_max = max(z)+1;


writerObj = VideoWriter('quad_video.avi');
open(writerObj);

figure(5); clf;
tic
t_real = toc;
for t = 1:3:length(phi)
    clf; hold on;
    
    plot3(sin(lambda),sin(2*cos(lambda)),cos(lambda), '--r','color','green','linewidth',1);
    hold on;
    
    plot3(x(1:t),y(1:t),z(1:t), 'r','color','red','linewidth',2);
    
    draw_quadrotor( qm_fnum, qm_xyz, phi(t), theta(t), ...
        psi(t), x(t), y(t), z(t),...
        [0.6 0.1 0.1]);
    %adjust rendering style
    lighting phong;
    camlight right;
    %adjust graph viewing aspects
    grid on;
    axis square;
    axis([axis_x_min, axis_x_max, axis_y_min, axis_y_max, axis_z_min, axis_z_max]);
    view(130,25);
    xlabel('X'); ylabel('Y'); zlabel('Z');
    %%% Uncomment the following two lines to record the video!!
    frame = getframe;
    writeVideo(writerObj,frame);
    
    drawnow;
end

close(writerObj);
toc