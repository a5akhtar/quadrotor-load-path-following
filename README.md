# quadrotor-load-path-following

- To run the code simply run "main_sin_xy.m".
- To turn on/off the noise, change the value of the variable noise to 1/0.
- To generate animation of this simulation run, please uncomment the code from lines 481 to 501.
