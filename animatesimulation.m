%% Cool simulator
tic
qm_matfilename = 'quad_starmac.mat';    %filename for the mat file for the quadrotor model
load(qm_matfilename);

axis_x_min = min([x;xb])-1;
axis_x_max = max([x;xb])+1;
axis_y_min = min([y;yb])-1;
axis_y_max = max([y;yb])+1;
axis_z_min = min([z;zb])-1;
axis_z_max = max([z;zb])+1;


writerObj = VideoWriter('quad_video.avi','Motion JPEG AVI');
writeObj.Quality = 100;
open(writerObj);

f = figure(5); 
clf;
tic
t_real = toc;
for t = 1:3:length(phi)
    clf; hold on;
    
    %plot3(sin(lambda),cos(lambda),(des_height+ amp*sin(freq*linspace(-1,1,2000))), '--r','color','green','linewidth',1);
    %plot3(sin(time(2:end-1)),cos(time(2:end-1)),(des_height + amp*sin(freq*xb)), '--r','color','green','linewidth',1);
    hold on;
    
    plot3(x(1:t),y(1:t),z(1:t), 'r','color','red','linewidth',2);
    
    plot3(xb(1:t),yb(1:t),zb(1:t), 'r','color','blue','linewidth',2);
    
    line ([x(t),xb(t)],[y(t),yb(t)],[z(t),zb(t)], 'color','blue','LineWidth',2);
    plot3(xb(t),yb(t),zb(t), 'bo', 'MarkerSize', 20, 'MarkerFaceColor', 'red');

    draw_quadrotor( qm_fnum, qm_xyz, phi(t), theta(t), ...
        psi(t), x(t), y(t), z(t),...
        [0.6 0.1 0.1]);
    %adjust rendering style
    lighting phong;
    camlight right;
    %adjust graph viewing aspects
    grid on;
    %axis square;
    axis([axis_x_min, axis_x_max, axis_y_min, axis_y_max, axis_z_min, axis_z_max]);
    view(130,25);
    xlabel('$x(m)$','FontSize',16,'Interpreter','latex')
    ylabel('$y(m)$','FontSize',16,'Interpreter','latex')
    zlabel('$z(m)$','FontSize',16,'Interpreter','latex')
    %%% Uncomment the following two lines to record the video!!
    frame = getframe(f);
    writeVideo(writerObj,frame);
    
    drawnow;
end

close(writerObj);
toc