%%% Genearate Plots 

t = time;

%% Plot the state of the system on (x,y,z) plane
    
% %%% displaying the initial marker
%     plot3(X1_0(k), X2_0(k), X3_0(k), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');
%     plot3(sin(lambda),sin(2*cos(lambda)),cos(lambda), '--r','color','green','linewidth',1);
%     hold on;
%     
%     plot3(x,y,z, 'r','color','red','linewidth',2);
%     
%     axis([-2 2 -2 2 -1 4]);
%     %title('Quadrotor Following a unit circle elevated at a height of 3-units')
%     xlabel('x_{1} = x')
%     ylabel('x_{3} = y')
%     zlabel('x_{5} = z')
%     grid on;
%     
%     plot3(X1_0(k), X2_0(k), X3_0(k), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');
% 
% 

% %%%%%%%%%%%%%%%%%%%%  Height of the quadrotor "z" %%%%%%%%%
% figure(3); 
% grid on;
% plot(t,z)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$z(m)$','FontSize',16,'Interpreter','latex')
% 
% 
% %% Body Rates
% %%%%%%%%%%%%%%%%%%%%% Body rate "p" %%%%%%%%%%%%%%%%
% figure(4);
% grid on;
% plot(t,p)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$p$','FontSize',16,'Interpreter','latex')
% 
% %%%%%%%%%%%%%%%%%%%%% Body rate "q" %%%%%%%%%%%%%%%%
% figure(5);
% grid on;
% plot(t,q)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$q$','FontSize',16,'Interpreter','latex')
% 
% %%%%%%%%%%%%%%%%%%%%% Body rate "r" %%%%%%%%%%%%%%%%
% figure(6);
% grid on;
% plot(t,r)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$r$','FontSize',16,'Interpreter','latex')
% 
%% Euler Angles
%%%%%%%%%%%%%%%%%%%%% Phi %%%%%%%%%%%%%%%%%%%%%%%%%
figure(7);
grid on;
plot(t,ph_graph)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\phi$','FontSize',16,'Interpreter','latex')

%%%%%%%%%%%%%%%%%%%%% Theta %%%%%%%%%%%%%%%%%%%%%%%%%
figure(8); 
grid on;
plot(t,th_graph)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\theta$','FontSize',16,'Interpreter','latex')

%%%%%%%%%%%%%%%%%%%%% Psi %%%%%%%%%%%%%%%%%%%%%%%%%
figure(9); 
grid on;
plot(t,ps_graph)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\psi$','FontSize',16,'Interpreter','latex')
% 
% 
% %% Plots for internal states and Internal Dynamics
% %%%%%%%%%%%%% First Internal state and Dynamics %%%%%%%%%%%%%%
% figure(10); 
% subplot(2,1,1) % first subplot
% plot(t,mu1, 'r','color','blue','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\mu_1$','FontSize',16,'Interpreter','latex')
% 
% subplot(2,1,2) % second subplot
% grid on;
% plot(t,mu1_d, 'r','color','red','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\dot\mu_1$','FontSize',16,'Interpreter','latex')
% 
% %%%%%%%%%%%%% Second Internal state and Dynamics %%%%%%%%%%%%%%
% figure(11); 
% subplot(2,1,1) % first subplot
% plot(t,mu2, 'r','color','blue','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\mu_2$','FontSize',16,'Interpreter','latex')
% 
% subplot(2,1,2) % second subplot
% grid on;
% plot(t,mu2_d, 'r','color','red','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\dot\mu_2$','FontSize',16,'Interpreter','latex')
% 
% 
% %%%%%%%%%%%%% Third Internal state and Dynamics %%%%%%%%%%%%%%
% figure(12); 
% subplot(2,1,1) % first subplot
% plot(t,mu3, 'r','color','blue','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\mu_3$','FontSize',16,'Interpreter','latex')
% 
% subplot(2,1,2) % second subplot
% grid on;
% plot(t,mu3_d, 'r','color','red','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\dot\mu_3$','FontSize',16,'Interpreter','latex')
% 
% 
% %%%%%%%%%%%%% Fourth Internal state and Dynamics %%%%%%%%%%%%%%
% figure(13); 
% subplot(2,1,1) % first subplot
% plot(t,mu4, 'r','color','blue','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\mu_4$','FontSize',16,'Interpreter','latex')
% 
% subplot(2,1,2) % second subplot
% grid on;
% plot(t,mu4_d, 'r','color','red','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\dot\mu_4$','FontSize',16,'Interpreter','latex')
% 
% %%%%%%%%%%%%% Fifth Internal state and Dynamics %%%%%%%%%%%%%%
% figure(14); 
% subplot(2,1,1) % first subplot
% plot(t,mu5, 'r','color','blue','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\mu_5$','FontSize',16,'Interpreter','latex')
% 
% subplot(2,1,2) % second subplot
% grid on;
% plot(t,mu5_d, 'r','color','red','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\dot\mu_5$','FontSize',16,'Interpreter','latex')
% 
% 
% %%%%%%%%%%%%% Sixth Internal state and Dynamics %%%%%%%%%%%%%%
% figure(15); 
% subplot(2,1,1) % first subplot
% plot(t,mu6, 'r','color','blue','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\mu_6$','FontSize',16,'Interpreter','latex')
% 
% subplot(2,1,2) % second subplot
% grid on;
% plot(t,mu6_d, 'r','color','red','linewidth',2)
% grid on;
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\dot\mu_6$','FontSize',16,'Interpreter','latex')

