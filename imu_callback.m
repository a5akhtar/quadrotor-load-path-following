function [State] = imu_callback(State)
    
%    State = [ xq; yq; zq; xb; yb; zb; vxq; vyq; vzq; vxb; vyb; vzb; ph; th; ps; p; q; r; zeta1;zeta2]
    
 
 Omega = State(16:18);
 
   %%% We assume std deviation (sigma = 0.1 degree)
   %%% This is a fair enough assumption considering we get filtered value
   %%% form the LLP, also it is mentioned in the following document
   %%% http://www.dis.uniroma1.it/~venditt/didattica/eir/02_HummingBird.pdf
   %%% A variance of 3.32 e-6 rads = 0.1 degree.

   R = rotationZ(State(15))*rotationY(State(14))*rotationX(State(13));
   %R = [r11 r12 r13; r21 r22 r23;r31 r32 r33];
   R_noise = expm( so3_hat(   randn(1,1).*1.0e-2*(pi/180)*[1;0;0]  + ...
                              randn(1,1).*1.0e-2*(pi/180)*[0;1;0]  + ...
                              randn(1,1).*1.0e-2*(pi/180)*[0;0;1]  ));

    R = R*R_noise;         
    
   [ph_noise,th_noise,ps_noise] = decompose_rotation(R);
   
   
   %%% We assume std deviation (sigma = 1 degree/sec)
   %%% This is a fair enough assumption considering we get filtered value
   %%% form the LLP
   Omega = Omega + [randn(1,1).*0.012; ...
                    randn(1,1).*0.012; ...
                    randn(1,1).*0.012];
             
             
             State(13:15) =  [ph_noise;th_noise;ps_noise];

             State(16:18) = Omega;

end