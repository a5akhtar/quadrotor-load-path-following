function [State] = ips_callback(State)

    
    %State = [ xq; yq; zq; xb; yb; zb; vxq; vyq; vzq; vxb; vyb; vzb; ph; th; ps; p; q; r; zeta1;zeta2];

    pos_q = State(1:3);
    vel_q = State(7:9);
    
    zeta = State(19:20);
                   
    pos_q =  pos_q + [randn(1,1).*1.0e-4 ; ...
              randn(1,1).*1.0e-4 ; ...
              randn(1,1).*1.0e-4 ];
                         
    vel_q =  vel_q + [randn(1,1).*3.0e-3 ; ...
              randn(1,1).*3.0e-3 ; ...
              randn(1,1).*3.0e-3 ]; 
          
          
    pos_b = State(4:6);
    vel_b = State(10:12);
                   
    pos_b =  pos_b + [randn(1,1).*1.0e-4 ; ...
              randn(1,1).*1.0e-4 ; ...
              randn(1,1).*1.0e-4 ];
                         
    vel_b =  vel_b + [randn(1,1).*3.0e-3 ; ...
              randn(1,1).*3.0e-3 ; ...
              randn(1,1).*3.0e-3 ];
          
    zeta =  zeta + [randn(1,1).*1.0e-5 ; ...
                    randn(1,1).*1.0e-1 ];      

   State(1:3) = pos_q;
   State(4:6) = pos_b;
   State(7:9) = vel_q;
   State(10:12) = vel_b;
   State(19:20) = zeta;
end