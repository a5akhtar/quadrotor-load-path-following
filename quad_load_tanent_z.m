clc
clear all
close all

rmpath Euler2RotationMatrix/
rmpath QuadAnimation/
addpath Euler2RotationMatrix/
addpath QuadAnimation/

%%% To turn on the sensor noise, set this  value to 1
noise = 0;

%%
TT = 20;
dt = 0.01; %%% Note: if the discretization step is 0.01, the system blows up!!
time = 0:dt:TT;
sim_length = length(time);

%% Initializing vectors with zeros 

        ph_graph = zeros(sim_length,1);
        th_graph = zeros(sim_length,1);
        ps_graph = zeros(sim_length,1);
        
        p_graph = zeros(sim_length,1);
        q_graph = zeros(sim_length,1);
        r_graph = zeros(sim_length,1);
        
        xq_graph = zeros(sim_length,1);
        yq_graph = zeros(sim_length,1);
        zq_graph = zeros(sim_length,1);
        vxq_graph = zeros(sim_length,1);
        vyq_graph = zeros(sim_length,1);
        vzq_graph = zeros(sim_length,1);

        xb_graph = zeros(sim_length,1);
        yb_graph = zeros(sim_length,1);
        zb_graph = zeros(sim_length,1);
        vxb_graph = zeros(sim_length,1);
        vyb_graph = zeros(sim_length,1);
        vzb_graph = zeros(sim_length,1);

        
        xi1_1_graph = zeros(sim_length,1);
        xi1_2_graph = zeros(sim_length,1);
        xi1_3_graph = zeros(sim_length,1);
        xi1_4_graph = zeros(sim_length,1);
        xi1_5_graph = zeros(sim_length,1);
        xi1_6_graph = zeros(sim_length,1);
        
        xi2_1_graph = zeros(sim_length,1);
        xi2_2_graph = zeros(sim_length,1);
        xi2_3_graph = zeros(sim_length,1);
        xi2_4_graph = zeros(sim_length,1);
        xi2_5_graph = zeros(sim_length,1);
        xi2_6_graph = zeros(sim_length,1);
        
        eta1_1_graph = zeros(sim_length,1);
        eta1_2_graph = zeros(sim_length,1);
        eta1_3_graph = zeros(sim_length,1);
        eta1_4_graph = zeros(sim_length,1);
        eta1_5_graph = zeros(sim_length,1);
        eta1_6_graph = zeros(sim_length,1);


        
        
        eta2_1_graph = zeros(sim_length,1);
        eta2_2_graph = zeros(sim_length,1);
        
        up_graph = zeros(sim_length,1);
        uq_graph = zeros(sim_length,1);
        ur_graph = zeros(sim_length,1);
        ut_graph = zeros(sim_length,1);
%% Initial conditions



% K1 = 1.0e+04 *[    5.6306    5.5097    2.2377    0.4828    0.0584    0.0038];
% K2 = 1.0e+04 *[    5.6306    5.5097    2.2377    0.4828    0.0584    0.0038];
% K3 = 1.0e+04 *[    5.6306    5.5097    2.2377    0.4828    0.0584    0.0038];

K1 = 1.0e+03 *[   2.6270    4.2843    2.9000    1.0429    0.2102    0.0225];
K2 = 1.0e+03 *[   2.6270    4.2843    2.9000    1.0429    0.2102    0.0225];
K3 = 1.0e+0 *[   230.6304  564.1920  572.8384  309.0000   93.4000   15.0000];
K4 = [27.5000   10.5000];



% K1 = 5.0*[ 1.7160    6.0260    7.9100    4.6000];
% K2 = 1.5*[ 1.7160    6.0260    7.9100    4.6000];
% K3 = 1.0*[1.7160    6.0260    7.9100    4.6000];

k1 = K1(1); %
k2 = K1(2);
k3 = K1(3);%
k4 = K1(4);
k5 = K1(5);
k6 = K1(6);


k7 = K2(1); %
k8 = K2(2);
k9 = K2(3);%
k10 = K2(4);
k11 = K2(5);
k12 = K2(6);


k13 = K3(1);
k14 = K3(2);
k15 = K3(3);
k16 = K3(4);
k17 = K3(5);
k18 = K3(6);

k19 = K4(1);
k20 = K4(2);%%% not used in this particular case

L= 1.5;

Ix = 0.0177;
Iy = 0.0177;
Iz = 0.0334;

g = 9.8;
mq = 0.1;
mb = 0.09;

kt = 0;
kr = 0;


% R_vec = reshape( R.', 1, 9 );

% R = diag([1,-1,-1]);
  ph = 0*(pi/180);
  th = 0*(pi/180);
  ps = 0*(pi/180);
%   R = rotationZ(ps)*rotationY(th)*rotationX(ph);
%  trace(R)
% 
% [ph,th,ps] = decompose_rotation(R);

    des_radius = 4;
    des_height = 10;
    des_yaw = pi/4;

vxq = 0.0;
vyq = 0;
vzq = 0.0;
xb = 0.0;
yb = 2.1;
zb = 10;

alpha = pi/600;
beta = pi/600;

xq = xb + L*cos(alpha)*sin(beta);
yq = yb + L*sin(alpha)*sin(beta);
zq = zb +  L*cos(beta);


vxb = 0.1;
vyb = 0.0;
vzb = 0;

p = 0.0;
q=  0.0;
r = 0.0;

Omega = [p;q;r];
zeta1 = 0.1;
zeta2 = 0.1;

    
    State = [ xq; yq; zq; xb; yb; zb; vxq; vyq; vzq; vxb; vyb; vzb; ph; th; ps; p; q; r; zeta1;zeta2];


    T = mb*g;
    current_time = 0;
    amp = 2;
    freq = 2.5;
%% Main simulation loop

for i = 1:TT/dt -1

    current_time = current_time + dt;
    
%     des_yaw = sin(current_time);
    
    %%%% Extracting state variables from the state vector
    xq = State(1);
    yq = State(2);
    zq = State(3);
    
    xb = State(4);
    yb = State(5);
    zb = State(6);
    
    
    vxq = State(7);
    vyq = State(8);
    vzq = State(9);
    
    vxb = State(10);
    vyb = State(11);
    vzb = State(12);

    ph = State(13);
    th = State(14);
    ps = State(15);
    
    p = State(16);
    q = State(17);
    r = State(18);
    ze1 = State(19);
    ze2 = State(20);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%  Transformed states %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    xi1_1 = yb - amp*sin(freq*xb);
    xi1_2 = vyb - amp*freq*vxb*cos(freq*xb);
    xi1_3 = amp*freq^2*vxb^2*sin(freq*xb) - (T*(yb - yq))/(L*mb) + (T*amp*freq*cos(freq*xb)*(xb - xq))/(L*mb);
    xi1_4 = (T*vyq - T*vyb + T*amp*freq*vxb*cos(freq*xb) - T*amp*freq*vxq*cos(freq*xb) - 3*T*amp*freq^2*vxb*xb*sin(freq*xb) + 3*T*amp*freq^2*vxb*xq*sin(freq*xb) + L*amp*freq^3*mb*vxb^3*cos(freq*xb))/(L*mb);
    xi1_5 = (T^2*(yb - yq))/(L^2*mb^2) - (vxb*(4*T*amp*freq^2*vxb*sin(freq*xb) - T*amp*freq^2*vxq*sin(freq*xb) + 3*T*amp*freq^3*vxb*xb*cos(freq*xb) - 3*T*amp*freq^3*vxb*xq*cos(freq*xb) + L*amp*freq^4*mb*vxb^3*sin(freq*xb)))/(L*mb) - (T*(xb - xq)*(T*amp*freq*cos(freq*xb) - 3*T*amp*freq^2*xb*sin(freq*xb) + 3*T*amp*freq^2*xq*sin(freq*xb) + 3*L*amp*freq^3*mb*vxb^2*cos(freq*xb)))/(L^2*mb^2) - (T*(zeta1*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)) - (T*(yb - yq))/L))/(L*mb*mq) - (T*amp*freq*cos(freq*xb)*(zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)) + (T*(xb - xq))/L))/(L*mb*mq) + (3*T*amp*freq^2*vxb*vxq*sin(freq*xb))/(L*mb);
    xi1_6 = -(T^2*mb*vyq - T^2*mb*vyb - T^2*mq*vyb + T^2*mq*vyq + T^2*amp*freq*mb*vxb*cos(freq*xb) - T^2*amp*freq*mb*vxq*cos(freq*xb) + T^2*amp*freq*mq*vxb*cos(freq*xb) - T^2*amp*freq*mq*vxq*cos(freq*xb) + L*T*mb*zeta2*cos(ps)*sin(ph) - L*T*mb*q*zeta1*cos(th)*sin(ps) - 5*T^2*amp*freq^2*mb*vxb*xb*sin(freq*xb) + 5*T^2*amp*freq^2*mb*vxb*xq*sin(freq*xb) - 15*T^2*amp*freq^2*mq*vxb*xb*sin(freq*xb) + 10*T^2*amp*freq^2*mq*vxq*xb*sin(freq*xb) + 15*T^2*amp*freq^2*mq*vxb*xq*sin(freq*xb) - 10*T^2*amp*freq^2*mq*vxq*xq*sin(freq*xb) - L*T*mb*zeta2*cos(ph)*sin(ps)*sin(th) - 15*T^2*amp*freq^3*mq*vxb*xb^2*cos(freq*xb) - 15*T^2*amp*freq^3*mq*vxb*xq^2*cos(freq*xb) + L^2*amp*freq^5*mb^2*mq*vxb^5*cos(freq*xb) + L*T*mb*p*zeta1*cos(ph)*cos(ps) + L*T*mb*p*zeta1*sin(ph)*sin(ps)*sin(th) + 10*L*T*amp*freq^3*mb*mq*vxb^3*cos(freq*xb) + 30*T^2*amp*freq^3*mq*vxb*xb*xq*cos(freq*xb) - 10*L*T*amp*freq^4*mb*mq*vxb^3*xb*sin(freq*xb) + 10*L*T*amp*freq^4*mb*mq*vxb^3*xq*sin(freq*xb) + L*T*amp*freq*mb*zeta2*cos(freq*xb)*sin(ph)*sin(ps) - 10*L*T*amp*freq^3*mb*mq*vxb^2*vxq*cos(freq*xb) + L*T*amp*freq*mb*q*zeta1*cos(freq*xb)*cos(ps)*cos(th) + L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*cos(ph)*sin(ps) + L*T*amp*freq*mb*zeta2*cos(freq*xb)*cos(ph)*cos(ps)*sin(th) - 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*sin(ph)*sin(ps) - L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*cos(ps)*sin(ph)*sin(th) - 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*cos(ph)*cos(ps)*sin(th))/(L^2*mb^2*mq);
    Lg1Lf5S1 = -(L*T*mb*cos(ps)*sin(ph) - L*T*mb*cos(ph)*sin(ps)*sin(th) + L*T*amp*freq*mb*cos(freq*xb)*sin(ph)*sin(ps) + L*T*amp*freq*mb*cos(freq*xb)*cos(ph)*cos(ps)*sin(th))/(L^2*mb^2*mq);
    Lg2Lf5S1 = -(L*T*mb*zeta1*cos(ph)*cos(ps) + L*T*mb*zeta1*sin(ph)*sin(ps)*sin(th) + L*T*amp*freq*mb*zeta1*cos(freq*xb)*cos(ph)*sin(ps) - L*T*amp*freq*mb*zeta1*cos(freq*xb)*cos(ps)*sin(ph)*sin(th))/(Ix*L^2*mb^2*mq);
    Lg3Lf5S1 = (L*T*mb*zeta1*cos(th)*sin(ps) - L*T*amp*freq*mb*zeta1*cos(freq*xb)*cos(ps)*cos(th))/(Iy*L^2*mb^2*mq);
    Lg4Lf5S1 = 0;
    Lf6S1 = (((r*cos(ph))/cos(th) + (q*sin(ph))/cos(th))*(L*T*mb*zeta2*sin(ph)*sin(ps) + L*T*mb*q*zeta1*cos(ps)*cos(th) + L*T*mb*p*zeta1*cos(ph)*sin(ps) + L*T*mb*zeta2*cos(ph)*cos(ps)*sin(th) - L*T*mb*p*zeta1*cos(ps)*sin(ph)*sin(th) - L*T*amp*freq*mb*zeta2*cos(freq*xb)*cos(ps)*sin(ph) - L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*cos(ph)*cos(ps) + L*T*amp*freq*mb*q*zeta1*cos(freq*xb)*cos(th)*sin(ps) + 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*cos(ps)*sin(ph) + L*T*amp*freq*mb*zeta2*cos(freq*xb)*cos(ph)*sin(ps)*sin(th) - L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*sin(ph)*sin(ps)*sin(th) - 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*cos(ph)*sin(ps)*sin(th)))/(L^2*mb^2*mq) - ((p + (r*cos(ph)*sin(th))/cos(th) + (q*sin(ph)*sin(th))/cos(th))*(L*T*mb*zeta2*cos(ph)*cos(ps) - L*T*mb*p*zeta1*cos(ps)*sin(ph) + L*T*mb*zeta2*sin(ph)*sin(ps)*sin(th) + L*T*mb*p*zeta1*cos(ph)*sin(ps)*sin(th) + L*T*amp*freq*mb*zeta2*cos(freq*xb)*cos(ph)*sin(ps) - L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*sin(ph)*sin(ps) - 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*cos(ph)*sin(ps) - L*T*amp*freq*mb*zeta2*cos(freq*xb)*cos(ps)*sin(ph)*sin(th) - L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*cos(ph)*cos(ps)*sin(th) + 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*cos(ps)*sin(ph)*sin(th)))/(L^2*mb^2*mq) + (vxb*(6*T^2*amp*freq^2*mb*vxb*sin(freq*xb) - T^2*amp*freq^2*mb*vxq*sin(freq*xb) + 16*T^2*amp*freq^2*mq*vxb*sin(freq*xb) - 11*T^2*amp*freq^2*mq*vxq*sin(freq*xb) + 5*T^2*amp*freq^3*mb*vxb*xb*cos(freq*xb) - 5*T^2*amp*freq^3*mb*vxb*xq*cos(freq*xb) + 45*T^2*amp*freq^3*mq*vxb*xb*cos(freq*xb) - 10*T^2*amp*freq^3*mq*vxq*xb*cos(freq*xb) - 45*T^2*amp*freq^3*mq*vxb*xq*cos(freq*xb) + 10*T^2*amp*freq^3*mq*vxq*xq*cos(freq*xb) - 15*T^2*amp*freq^4*mq*vxb*xb^2*sin(freq*xb) - 15*T^2*amp*freq^4*mq*vxb*xq^2*sin(freq*xb) + L^2*amp*freq^6*mb^2*mq*vxb^5*sin(freq*xb) + 20*L*T*amp*freq^4*mb*mq*vxb^3*sin(freq*xb) + 30*T^2*amp*freq^4*mq*vxb*xb*xq*sin(freq*xb) + L*T*amp*freq^2*mb*zeta2*sin(freq*xb)*sin(ph)*sin(ps) + 10*L*T*amp*freq^5*mb*mq*vxb^3*xb*cos(freq*xb) - 10*L*T*amp*freq^5*mb*mq*vxb^3*xq*cos(freq*xb) - 10*L*T*amp*freq^4*mb*mq*vxb^2*vxq*sin(freq*xb) + L*T*amp*freq^2*mb*zeta2*sin(freq*xb)*cos(ph)*cos(ps)*sin(th) + L*T*amp*freq^2*mb*q*zeta1*sin(freq*xb)*cos(ps)*cos(th) + L*T*amp*freq^2*mb*p*zeta1*sin(freq*xb)*cos(ph)*sin(ps) + 5*L*T*amp*freq^3*mb*vxb*zeta1*cos(freq*xb)*sin(ph)*sin(ps) + 5*L*T*amp*freq^3*mb*vxb*zeta1*cos(freq*xb)*cos(ph)*cos(ps)*sin(th) - L*T*amp*freq^2*mb*p*zeta1*sin(freq*xb)*cos(ps)*sin(ph)*sin(th)))/(L^2*mb^2*mq) + ((q*cos(ph) - r*sin(ph))*(L*T*mb*zeta2*cos(ph)*cos(th)*sin(ps) - L*T*mb*q*zeta1*sin(ps)*sin(th) - L*T*mb*p*zeta1*cos(th)*sin(ph)*sin(ps) + L*T*amp*freq*mb*q*zeta1*cos(freq*xb)*cos(ps)*sin(th) - L*T*amp*freq*mb*zeta2*cos(freq*xb)*cos(ph)*cos(ps)*cos(th) + L*T*amp*freq*mb*p*zeta1*cos(freq*xb)*cos(ps)*cos(th)*sin(ph) + 5*L*T*amp*freq^2*mb*vxb*zeta1*sin(freq*xb)*cos(ph)*cos(ps)*cos(th)))/(L^2*mb^2*mq) + ((zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)) + (T*(xb - xq))/L)*(T^2*amp*freq*mb*cos(freq*xb) + T^2*amp*freq*mq*cos(freq*xb) - 10*T^2*amp*freq^2*mq*xb*sin(freq*xb) + 10*T^2*amp*freq^2*mq*xq*sin(freq*xb) + 10*L*T*amp*freq^3*mb*mq*vxb^2*cos(freq*xb)))/(L^2*mb^2*mq^2) + ((T^2*mb + T^2*mq)*(zeta1*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)) - (T*(yb - yq))/L))/(L^2*mb^2*mq^2) - (zeta2*(L*T*mb*p*cos(ph)*cos(ps) - L*T*mb*q*cos(th)*sin(ps) + L*T*mb*p*sin(ph)*sin(ps)*sin(th) - 5*L*T*amp*freq^2*mb*vxb*sin(freq*xb)*sin(ph)*sin(ps) + L*T*amp*freq*mb*q*cos(freq*xb)*cos(ps)*cos(th) + L*T*amp*freq*mb*p*cos(freq*xb)*cos(ph)*sin(ps) - 5*L*T*amp*freq^2*mb*vxb*sin(freq*xb)*cos(ph)*cos(ps)*sin(th) - L*T*amp*freq*mb*p*cos(freq*xb)*cos(ps)*sin(ph)*sin(th)))/(L^2*mb^2*mq) - (vxq*(5*T^2*amp*freq^2*mb*vxb*sin(freq*xb) + 15*T^2*amp*freq^2*mq*vxb*sin(freq*xb) - 10*T^2*amp*freq^2*mq*vxq*sin(freq*xb) + 30*T^2*amp*freq^3*mq*vxb*xb*cos(freq*xb) - 30*T^2*amp*freq^3*mq*vxb*xq*cos(freq*xb) + 10*L*T*amp*freq^4*mb*mq*vxb^3*sin(freq*xb)))/(L^2*mb^2*mq) - (T*(T^2*mb + T^2*mq)*(yb - yq))/(L^3*mb^3*mq) - ((Iy*q*r - Iz*q*r)*(L*T*mb*zeta1*cos(ph)*cos(ps) + L*T*mb*zeta1*sin(ph)*sin(ps)*sin(th) + L*T*amp*freq*mb*zeta1*cos(freq*xb)*cos(ph)*sin(ps) - L*T*amp*freq*mb*zeta1*cos(freq*xb)*cos(ps)*sin(ph)*sin(th)))/(Ix*L^2*mb^2*mq) + (T*(xb - xq)*(T^2*amp*freq*mb*cos(freq*xb) + T^2*amp*freq*mq*cos(freq*xb) - 15*T^2*amp*freq^3*mq*xb^2*cos(freq*xb) - 15*T^2*amp*freq^3*mq*xq^2*cos(freq*xb) - 5*T^2*amp*freq^2*mb*xb*sin(freq*xb) + 5*T^2*amp*freq^2*mb*xq*sin(freq*xb) - 15*T^2*amp*freq^2*mq*xb*sin(freq*xb) + 15*T^2*amp*freq^2*mq*xq*sin(freq*xb) + 30*T^2*amp*freq^3*mq*xb*xq*cos(freq*xb) + 5*L^2*amp*freq^5*mb^2*mq*vxb^4*cos(freq*xb) + 30*L*T*amp*freq^3*mb*mq*vxb^2*cos(freq*xb) - 30*L*T*amp*freq^4*mb*mq*vxb^2*xb*sin(freq*xb) + 30*L*T*amp*freq^4*mb*mq*vxb^2*xq*sin(freq*xb) - 5*L*T*amp*freq^2*mb*zeta1*sin(freq*xb)*sin(ph)*sin(ps) - 20*L*T*amp*freq^3*mb*mq*vxb*vxq*cos(freq*xb) - 5*L*T*amp*freq^2*mb*zeta1*sin(freq*xb)*cos(ph)*cos(ps)*sin(th)))/(L^3*mb^3*mq) - ((Ix*p*r - Iz*p*r)*(L*T*mb*zeta1*cos(th)*sin(ps) - L*T*amp*freq*mb*zeta1*cos(freq*xb)*cos(ps)*cos(th)))/(Iy*L^2*mb^2*mq);
    xi2_1 = zb - des_height;
    xi2_2 = vzb;
    xi2_3 = -(g*mb + (T*(zb - zq))/L)/mb;
    xi2_4 = -(T*(vzb - vzq))/(L*mb);
    xi2_5 = (T*(T*mb*zb - T*mb*zq + T*mq*zb - T*mq*zq + L*mb*zeta1*cos(ph)*cos(th)))/(L^2*mb^2*mq);
    xi2_6 = -(T*(T*mb*vzq - T*mb*vzb - T*mq*vzb + T*mq*vzq - L*mb*zeta2*cos(ph)*cos(th) + L*mb*q*zeta1*sin(th) + L*mb*p*zeta1*cos(th)*sin(ph)))/(L^2*mb^2*mq);
    Lg1Lf5S2 = (T*cos(ph)*cos(th))/(L*mb*mq);
    Lg2Lf5S2 = -(T*zeta1*cos(th)*sin(ph))/(Ix*L*mb*mq);
    Lg3Lf5S2 = -(T*zeta1*sin(th))/(Iy*L*mb*mq);
    Lg4Lf5S2 = 0;
    Lf6S2 = (T*zeta1*sin(th)*(Ix*p*r - Iz*p*r))/(Iy*L*mb*mq) - (T*(T*mb + T*mq)*(g*mb + (T*(zb - zq))/L))/(L^2*mb^3*mq) - (T*zeta2*(L*mb*q*sin(th) + L*mb*p*cos(th)*sin(ph)))/(L^2*mb^2*mq) - (T*(L*mb*zeta2*cos(th)*sin(ph) + L*mb*p*zeta1*cos(ph)*cos(th))*(p + (r*cos(ph)*sin(th))/cos(th) + (q*sin(ph)*sin(th))/cos(th)))/(L^2*mb^2*mq) - (T*(T*mb + T*mq)*((T*(zb - zq))/L - g*mq + zeta1*cos(ph)*cos(th)))/(L^2*mb^2*mq^2) - (T*(q*cos(ph) - r*sin(ph))*(L*mb*zeta2*cos(ph)*sin(th) + L*mb*q*zeta1*cos(th) - L*mb*p*zeta1*sin(ph)*sin(th)))/(L^2*mb^2*mq) - (T*zeta1*cos(th)*sin(ph)*(Iy*q*r - Iz*q*r))/(Ix*L*mb*mq);
    eta1_1 = xb;
    eta1_2 = vxb;
    eta1_3 = -(T*(xb - xq))/(L*mb);
    eta1_4 = -(T*(vxb - vxq))/(L*mb);
    eta1_5 = (T^2*(xb - xq))/(L^2*mb^2) + (T*(zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)) + (T*(xb - xq))/L))/(L*mb*mq);
    eta1_6 = (T^2*vxb*(mb + mq))/(L^2*mb^2*mq) - (T^2*vxq*(mb + mq))/(L^2*mb^2*mq) + (T*zeta2*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)))/(L*mb*mq) + (T*zeta1*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(p*cos(th) + r*cos(ph)*sin(th) + q*sin(ph)*sin(th)))/(L*mb*mq*cos(th)) + (T*zeta1*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(r*cos(ph) + q*sin(ph)))/(L*mb*mq*cos(th)) + (T*zeta1*cos(ph)*cos(ps)*cos(th)*(q*cos(ph) - r*sin(ph)))/(L*mb*mq);
    Lg1Lf5P1 = (T*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)))/(L*mb*mq);
    Lg2Lf5P1 = (T*zeta1*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th)))/(Ix*L*mb*mq);
    Lg3Lf5P1 = ((T*zeta1*sin(ph)*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)))/(L*mb*mq*cos(th)) + (T*zeta1*cos(ph)^2*cos(ps)*cos(th))/(L*mb*mq) + (T*zeta1*sin(ph)*sin(th)*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th)))/(L*mb*mq*cos(th)))/Iy;
    Lg4Lf5P1 = ((T*zeta1*cos(ph)*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)))/(L*mb*mq*cos(th)) - (T*zeta1*cos(ph)*cos(ps)*cos(th)*sin(ph))/(L*mb*mq) + (T*zeta1*cos(ph)*sin(th)*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th)))/(L*mb*mq*cos(th)))/Iz;
    Lf6P1 = (p + (r*cos(ph)*sin(th))/cos(th) + (q*sin(ph)*sin(th))/cos(th))*((T*zeta2*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th)))/(L*mb*mq) - (T*zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th))*(p*cos(th) + r*cos(ph)*sin(th) + q*sin(ph)*sin(th)))/(L*mb*mq*cos(th)) + (T*zeta1*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(q*cos(ph)*sin(th) - r*sin(ph)*sin(th)))/(L*mb*mq*cos(th)) + (T*zeta1*(cos(ph)*cos(ps) + sin(ph)*sin(ps)*sin(th))*(r*cos(ph) + q*sin(ph)))/(L*mb*mq*cos(th)) + (T*zeta1*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(q*cos(ph) - r*sin(ph)))/(L*mb*mq*cos(th)) - (T*zeta1*cos(ph)*cos(ps)*cos(th)*(r*cos(ph) + q*sin(ph)))/(L*mb*mq) - (T*zeta1*cos(ps)*cos(th)*sin(ph)*(q*cos(ph) - r*sin(ph)))/(L*mb*mq)) + (q*cos(ph) - r*sin(ph))*((T*zeta1*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(r*cos(ph)*cos(th) - p*sin(th) + q*cos(th)*sin(ph)))/(L*mb*mq*cos(th)) - (T*zeta1*cos(ps)*sin(ph)*(p*cos(th) + r*cos(ph)*sin(th) + q*sin(ph)*sin(th)))/(L*mb*mq) - (T*zeta1*cos(ph)*sin(ps)*(r*cos(ph) + q*sin(ph)))/(L*mb*mq) + (T*zeta2*cos(ph)*cos(ps)*cos(th))/(L*mb*mq) + (T*zeta1*sin(th)*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(p*cos(th) + r*cos(ph)*sin(th) + q*sin(ph)*sin(th)))/(L*mb*mq*cos(th)^2) - (T*zeta1*cos(ph)*cos(ps)*sin(th)*(q*cos(ph) - r*sin(ph)))/(L*mb*mq) + (T*zeta1*sin(th)*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(r*cos(ph) + q*sin(ph)))/(L*mb*mq*cos(th)^2)) + zeta2*((T*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(r*cos(ph) + q*sin(ph)))/(L*mb*mq*cos(th)) + (T*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(p*cos(th) + r*cos(ph)*sin(th) + q*sin(ph)*sin(th)))/(L*mb*mq*cos(th)) + (T*cos(ph)*cos(ps)*cos(th)*(q*cos(ph) - r*sin(ph)))/(L*mb*mq)) + ((r*cos(ph))/cos(th) + (q*sin(ph))/cos(th))*((T*zeta2*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)))/(L*mb*mq) + (T*zeta1*(cos(ph)*cos(ps) + sin(ph)*sin(ps)*sin(th))*(p*cos(th) + r*cos(ph)*sin(th) + q*sin(ph)*sin(th)))/(L*mb*mq*cos(th)) - (T*zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th))*(r*cos(ph) + q*sin(ph)))/(L*mb*mq*cos(th)) - (T*zeta1*cos(ph)*cos(th)*sin(ps)*(q*cos(ph) - r*sin(ph)))/(L*mb*mq)) - ((Ix*p*r - Iz*p*r)*((T*zeta1*sin(ph)*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)))/(L*mb*mq*cos(th)) + (T*zeta1*cos(ph)^2*cos(ps)*cos(th))/(L*mb*mq) + (T*zeta1*sin(ph)*sin(th)*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th)))/(L*mb*mq*cos(th))))/Iy + ((Ix*p*q - Iy*p*q)*((T*zeta1*cos(ph)*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)))/(L*mb*mq*cos(th)) - (T*zeta1*cos(ph)*cos(ps)*cos(th)*sin(ph))/(L*mb*mq) + (T*zeta1*cos(ph)*sin(th)*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th)))/(L*mb*mq*cos(th))))/Iz - (T^3*(mb + mq)*(xb - xq))/(L^3*mb^3*mq) - (T^2*(mb + mq)*(zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)) + (T*(xb - xq))/L))/(L^2*mb^2*mq^2) + (T*zeta1*(cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(Iy*q*r - Iz*q*r))/(Ix*L*mb*mq);
    eta2_1 = ps - des_yaw;
    eta2_2 = (r*cos(ph) + q*sin(ph))/cos(th);
    Lg1LfP2 = 0;
    Lg2LfP2 = 0;
    Lg3LfP2 = sin(ph)/(Iy*cos(th));
    Lg4LfP2 = cos(ph)/(Iz*cos(th));
    Lf2P2 = ((q*cos(ph) - r*sin(ph))*(p + (r*cos(ph)*sin(th))/cos(th) + (q*sin(ph)*sin(th))/cos(th)))/cos(th) + (cos(ph)*(Ix*p*q - Iy*p*q))/(Iz*cos(th)) - (sin(ph)*(Ix*p*r - Iz*p*r))/(Iy*cos(th)) + (sin(th)*(r*cos(ph) + q*sin(ph))*(q*cos(ph) - r*sin(ph)))/cos(th)^2;

      
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    D = [Lg1Lf5S1 Lg2Lf5S1 Lg3Lf5S1  Lg4Lf5S1; 
         Lg1Lf5S2 Lg2Lf5S2  Lg3Lf5S2 Lg4Lf5S2; 
         Lg1Lf5P1 Lg2Lf5P1  Lg3Lf5P1 Lg4Lf5P1; 
         Lg1LfP2  Lg2LfP2   Lg3LfP2  Lg4LfP2];
        
    det_d = det(D)
    
    v1_tran=  -Lf6S1 -k1*xi1_1 - k2*xi1_2 - k3*xi1_3 - k4*xi1_4 - k5*xi1_5 - k6*xi1_6;
    v2_tran=  -Lf6S2 -k7*xi2_1 - k8*xi2_2 - k9*xi2_3 - k10*xi2_4 - k11*xi2_5 - k12*xi2_6;
    
%   if (current_time <10)
    v1_tang = -Lf6P1  -k14*(eta1_2 - 0.5) -k15*eta1_3 -k16*eta1_4 - k17*eta1_5 - k18*eta1_6;
%     end
%     if(current_time >=10 && current_time< 15)
%         v1_tang = -Lf6P1  -k14*(eta1_2 - 0) -k15*eta1_3 -k16*eta1_4 - k17*eta1_5 - k18*eta1_6;
%     end
%     if(current_time >= 15)
%         v1_tang = -Lf6P1  -k14*(eta1_2 + 0.5) -k15*eta1_3 -k16*eta1_4 - k17*eta1_5 - k18*eta1_6;
%     end
    
    v2_tang = -Lf2P2 -k19*(eta2_1 - des_yaw) - k20*(eta2_2);

    
    u=D\[v1_tran; v2_tran; v1_tang; v2_tang];
    
    u_new=u(1);
    taup=u(2);
    tauq=u(3);
    taur=u(4);

%     v1_tang = -Lf6P1  -k14*(eta1_2 - 0.5) -k15*eta1_3 -k16*eta1_4 - k17*eta1_5 - k18*eta1_6;

    up = taup;
    uq = tauq;
    ur = taur;
    

    Omega = [p;q;r];
    %%%% System Dynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
  xq = xq + dt*vxq;
  yq = yq + dt*vyq;
  zq = zq + dt*vzq;
  xb = xb + dt*vxb;
  yb = yb + dt*vyb;
  zb = zb + dt*vzb;
  vxq = vxq + (dt*(zeta1*(sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th)) + (T*(xb - xq))/L))/mq;
  vyq = vyq - (dt*(zeta1*(cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th)) - (T*(yb - yq))/L))/mq;
  vzq = vzq + (dt*((T*(zb - zq))/L - g*mq + zeta1*cos(ph)*cos(th)))/mq;
  vxb = vxb - (T*dt*(xb - xq))/(L*mb);
  vyb = vyb - (T*dt*(yb - yq))/(L*mb);
  vzb = vzb - (dt*(g*mb + (T*(zb - zq))/L))/mb;
  ph = ph + dt*(p + (r*cos(ph)*sin(th))/cos(th) + (q*sin(ph)*sin(th))/cos(th));
  th = th + dt*(q*cos(ph) - r*sin(ph));
  ps = ps + dt*((r*cos(ph))/cos(th) + (q*sin(ph))/cos(th));
  p = p + (dt*(up + Iy*q*r - Iz*q*r))/Ix;
  q = q + (dt*(uq - Ix*p*r + Iz*p*r))/Iy;
  r =r + (dt*(ur + Ix*p*q - Iy*p*q))/Iz;
  zeta1 = zeta1 + dt*zeta2;
  zeta2 = zeta2 + dt*u_new;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    State = [ xq; yq; zq; xb; yb; zb; vxq; vyq; vzq; vxb; vyb; vzb; ph; th; ps; p; q; r; zeta1;zeta2];

    Omega = [p;q;r];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%% saving data for plotting purposes %%%%%%%%%%%%%%%%%%
    ph_graph(i) = ph;
    th_graph(i) = th;
    ps_graph(i) = ps;
    
    p_graph(i) = Omega(1);
    q_graph(i) = Omega(2);
    r_graph(i) = Omega(3);
    
    xq_graph(i) = xq;
    yq_graph(i) = yq;
    zq_graph(i) = zq;
    vxq_graph(i) = vxq;
    vyq_graph(i) = vyq;
    vzq_graph(i) = vzq;
    
    xb_graph(i) = xb;
    yb_graph(i) = yb;
    zb_graph(i) = zb;
    vxb_graph(i) = vxb;
    vyb_graph(i) = vyb;
    vzb_graph(i) = vzb;

    
    xi1_1_graph(i) = xi1_1;
    xi1_2_graph(i) = xi1_2;
    xi1_3_graph(i) = xi1_3;
    xi1_4_graph(i) = xi1_4;
    xi1_5_graph(i) = xi1_5;
    xi1_6_graph(i) = xi1_6;
    
    xi2_1_graph(i) = xi2_1;
    xi2_2_graph(i) = xi2_2;
    xi2_3_graph(i) = xi2_3;
    xi2_4_graph(i) = xi2_4;
    xi2_5_graph(i) = xi2_5;
    xi2_6_graph(i) = xi2_6;
    
    eta1_1_graph(i) = eta1_1;
    eta1_2_graph(i) = eta1_2;
    eta1_3_graph(i) = eta1_3;
    eta1_4_graph(i) = eta1_4;
    eta1_5_graph(i) = eta1_5;
    eta1_6_graph(i) = eta1_6;

    eta2_1_graph(i) = eta2_1;
    eta2_2_graph(i) = eta2_2;
    %%%%%  control input storage %%%%%%%%
    up_graph(i) = up;
    uq_graph(i) = uq;
    ur_graph(i) = ur;
    ut_graph(i) = zeta1;
    
    
    
    if (i > 1)
        load_acc = [(vxb_graph(i-1) - vxb_graph(i));(vyb_graph(i-1) - vyb_graph(i));(vzb_graph(i-1) - vzb_graph(i))]./dt;
        Tp = -mb*load_acc - mb*g*[0;0;1];
        mag_Tp = Tp./(norm(Tp));
        P_load = (1/L)*(State(4:6) - State(1:3));
         T = Tp'*P_load;
         
    end
    T = mb*g;
    T_graph(i)= T;
end
    
% %%% displaying the initial marker
%     plot3(X1_0(k), X2_0(k), X3_0(k), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');
%     plot3(sin(lambda),sin(2*cos(lambda)),cos(lambda), '--r','color','green','linewidth',1);
%     hold on;
%     
%     plot3(x,y,z, 'r','color','red','linewidth',2);
%     
%     axis([-2 2 -2 2 -1 4]);
%     %title('Quadrotor Following a unit circle elevated at a height of 3-units')
%     xlabel('x_{1} = x')
%     ylabel('x_{3} = y')
%     zlabel('x_{5} = z')
%     grid on;
%     
%     plot3(X1_0(k), X2_0(k), X3_0(k), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');

lambda = 0:0.01:4*pi;
ss = size(lambda)
z_lambda = zeros(1,ss(2)) + 1;
figure;


% figure()
% hold on;
% plot3(xq_graph(1),yq_graph(1),zq_graph(1), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');
% plot3(cos(time),sin(time), (time.*0 + 10 + amp*sin(freq*cos(time))), '--r','color','green','linewidth',3);
% plot3(xq_graph(1:end-2),yq_graph(1:end-2),zq_graph(1:end-2), 'r','color','red','linewidth',2);
% plot3(xb_graph(1:end-2),yb_graph(1:end-2),zb_graph(1:end-2), '--r','color','red','linewidth',2);
% % axis([-2 2 -2 2 -1 12]);
% legend({'$\chi(0)$','$\gamma$','$h(\chi)$'},'FontSize',14,'Interpreter','latex')
% xlabel('$x(m)$','FontSize',16,'Interpreter','latex')
% ylabel('$y(m)$','FontSize',16,'Interpreter','latex')
% zlabel('$z(m)$','FontSize',16,'Interpreter','latex')
% grid on;


t = time;

qm_matfilename = 'quad_starmac.mat';    %filename for the mat file for the quadrotor model
load(qm_matfilename);


figure()

plot3(4*cos(time),4*sin(time), (time.*0 + 10 + amp*sin(freq*atan2(sin(time),cos(time)))), '--r','color','green','linewidth',3);
hold on;
plot3(xq_graph(1:end-2),yq_graph(1:end-2),zq_graph(1:end-2), 'r','color','red','linewidth',2);
%plot3(xq_graph(50:100:end-2),yq_graph(50:100:end-2),zq_graph(50:100:end-2), 'r>','color','red','linewidth',2,'marker','>');
plot3(xb_graph(1:end-2),yb_graph(1:end-2),zb_graph(1:end-2), '--r','color','blue','linewidth',2);
%plot3(xb_graph(1),yb_graph(1),zb_graph(1), 'd', 'MarkerSize', 10, 'MarkerFaceColor', 'red');

for (ind = 1:100:length(time) -1)
line ([xq_graph(ind),xb_graph(ind)],[yq_graph(ind),yb_graph(ind)],[zq_graph(ind),zb_graph(ind)], 'color',[0.3 0.3 0.3],'LineWidth',1);
plot3(xb_graph(ind),yb_graph(ind),zb_graph(ind), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', [0.0 0.0 0.0]);
    draw_quadrotor( ceil(0.8*qm_fnum), 0.5*qm_xyz, ph_graph(ind), th_graph(ind), ...
        ps_graph(ind), xq_graph(ind), yq_graph(ind), zq_graph(ind),...
        [0.3 0.3 0.3]);
end
% axis([-2 2 -2 2 -1 12]);
legend({'$\gamma$','$x_{q}$','$x_{\ell}$','cable','load','quadrotor'},'FontSize',14,'Interpreter','latex')
xlabel('$x(m)$','FontSize',16,'Interpreter','latex')
ylabel('$y(m)$','FontSize',16,'Interpreter','latex')
zlabel('$z(m)$','FontSize',16,'Interpreter','latex')
grid on;

figure();
plot(time, (time.*0 + 10 + amp*sin(freq*time)), '--r','color','green','linewidth',3);
hold on;
plot(time(1:end-1),zb_graph(1:end-1), '--r','color','red','linewidth',2);
grid on;

figure()
ind = 1;
plot3(xb_graph(ind),yb_graph(ind),zb_graph(ind), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', [0.0 0.0 0.0]);
hold on;
    draw_quadrotor( qm_fnum, 0.5*qm_xyz, ph_graph(ind), th_graph(ind), ...
        ps_graph(ind), xq_graph(ind), yq_graph(ind), zq_graph(ind),...
        [0.3 0.3 0.3]);
line ([xq_graph(ind),xb_graph(ind)],[yq_graph(ind),yb_graph(ind)],[zq_graph(ind),zb_graph(ind)], 'color',[0.3 0.3 0.3],'LineWidth',1);
xlim([xq_graph(ind)-2 xq_graph(ind)+2]);
ylim([xq_graph(ind)-2 xq_graph(ind)+2]);




% %%%%%%%%%%%%%%%%%%%%%%%%%%%% xi plots %%%%%%%%%%%%%%%%%%%%%%%%%%%
figure()
subplot(2,1,1)
hold on;
plot(time(1:end-2),xi1_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),xi1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),xi1_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),xi1_4_graph(1:end-2), 'r','color','black','linewidth',2)
plot(time(1:end-2),xi1_5_graph(1:end-2), 'r','color','magenta','linewidth',2)
plot(time(1:end-2),xi1_6_graph(1:end-2), 'r','color','cyan','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\xi$-states','FontSize',16,'Interpreter','latex')
legend({'$\xi_{1}$','$\xi_{2}$','$\xi_{3}$','$\xi_{4}$','$\xi_{5}$','$\xi_{6}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;

subplot(2,1,2)

hold on;
plot(time(1:end-2),xi2_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),xi2_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),xi2_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),xi2_4_graph(1:end-2), 'r','color','black','linewidth',2)
plot(time(1:end-2),xi2_5_graph(1:end-2), 'r','color','magenta','linewidth',2)
plot(time(1:end-2),xi2_6_graph(1:end-2), 'r','color','cyan','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\zeta$-states','FontSize',16,'Interpreter','latex')
legend({'$\zeta_{1}$','$\zeta_{2}$','$\zeta_{3}$','$\zeta_{4}$','$\zeta_{5}$','$\zeta_{6}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;

figure()
subplot(2,1,1)

hold on;
plot(time(1:end-2),eta1_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),eta1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),eta1_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),eta1_4_graph(1:end-2), 'r','color','black','linewidth',2)
plot(time(1:end-2),eta1_5_graph(1:end-2), 'r','color','magenta','linewidth',2)
plot(time(1:end-2),eta1_6_graph(1:end-2), 'r','color','cyan','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\eta$-states','FontSize',16,'Interpreter','latex')
legend({'$\eta_{1}$','$\eta_{2}$','$\eta_{3}$','$\eta_{4}$','$\eta_{5}$','$\eta_{6}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;

subplot(2,1,2)

hold on;
plot(time(1:end-2),eta2_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),eta2_2_graph(1:end-2), 'r','color','blue','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\mu$-states','FontSize',16,'Interpreter','latex')
legend({'$\mu_{1}$','$\mu_{2}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;





% 
% 
% 
% figure()
% hold on;
% subplot(3,1,1)
% plot(time,vx_graph, 'r','color','blue','linewidth',2)
% xlabel('$t(sec)$','FontSize',14,'Interpreter','latex')
% ylabel('$v_x (m/sec)$','FontSize',14,'Interpreter','latex')
% grid on;
% subplot(3,1,2)
% plot(time,vy_graph, 'r','color','green','linewidth',2);
% xlabel('$t(sec)$','FontSize',14,'Interpreter','latex')
% ylabel('$v_y(m/sec)$','FontSize',14,'Interpreter','latex')
% grid on;
% subplot(3,1,3)
% plot(time,vz_graph, 'r','color','red','linewidth',2);
% xlabel('$t(sec)$','FontSize',14,'Interpreter','latex')
% ylabel('$v_z(m/sec)$','FontSize',14,'Interpreter','latex')
% grid on;
% 
% 
% 
% 
% 
% 
% figure()
% plot3(x_graph(1:end-2),y_graph(1:end-2),z_graph(1:end-2), 'r','color','red','linewidth',2);
% 
% figure()
% hold on;
% plot(time(1:end-2),xi1_1_graph(1:end-2), 'r','color','red','linewidth',2)
% plot(time(1:end-2),xi1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
% plot(time(1:end-2),xi1_3_graph(1:end-2), 'r','color','green','linewidth',2)
% plot(time(1:end-2),xi1_4_graph(1:end-2), 'r','color','black','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\xi_{1i}$','FontSize',16,'Interpreter','latex')
% grid on;
% 
% 
% figure()
% hold on;
% plot(time(1:end-2),xi2_1_graph(1:end-2), 'r','color','red','linewidth',2)
% plot(time(1:end-2),xi2_2_graph(1:end-2), 'r','color','blue','linewidth',2)
% plot(time(1:end-2),xi2_3_graph(1:end-2), 'r','color','green','linewidth',2)
% plot(time(1:end-2),xi2_4_graph(1:end-2), 'r','color','black','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\xi_{2i}$','FontSize',16,'Interpreter','latex')
% grid on;
% 
% figure()
% hold on;
% plot(time(1:end-2),eta1_1_graph(1:end-2), 'r','color','red','linewidth',2)
% plot(time(1:end-2),eta1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
% plot(time(1:end-2),eta1_3_graph(1:end-2), 'r','color','green','linewidth',2)
% plot(time(1:end-2),eta1_4_graph(1:end-2), 'r','color','black','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\eta_{1i}$','FontSize',16,'Interpreter','latex')
% grid on;
% 
% figure()
% hold on;
% plot(time(1:end-2),eta2_1_graph(1:end-2), 'r','color','red','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\eta_{21}$','FontSize',16,'Interpreter','latex')
% grid on;
% 
% % figure();
% % plot(trace_R);
% % title('trace(R)')
% % grid on;
% % 
% % figure();
% % plot(det_D);
% % title('det(D)')
% % grid on;
% 
% % figure();
% % plot(decoupling_condition);
% % title('trace (R_d*R)')
% % grid on;
% 
% % figure()
% % plot(time(1:end-2),error, 'r','color','green','linewidth',2)
% % xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% % ylabel('$tr(I-R_dR)$','FontSize',16,'Interpreter','latex')
% % grid on;
% % 
% % 
% % figure()
% % plot(time(1:end-2),det_R, 'r','color','green','linewidth',2)
% % xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% % ylabel('$det(R)$','FontSize',16,'Interpreter','latex')
% % grid on;
% 
% %%
% 
% figure()
% hold on;
% plot(time(1:end-2),ph_graph(1:end-2)*(180/pi), 'r','color','blue','linewidth',2)
% plot(time(1:end-2),th_graph(1:end-2)*(180/pi), 'r','color','green','linewidth',2);
% plot(time(1:end-2),ps_graph(1:end-2)*(180/pi), 'r','color','red','linewidth',2);
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\phi,\theta, \psi (degree) $','FontSize',16,'Interpreter','latex')
% legend('\phi','\theta', '\psi');
% grid on;
% 
% figure()
% hold on;
% plot(time(1:end-2),p_graph(1:end-2), 'r','color','blue','linewidth',2)
% plot(time(1:end-2),q_graph(1:end-2), 'r','color','green','linewidth',2);
% plot(time(1:end-2),r_graph(1:end-2), 'r','color','red','linewidth',2);
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\Omega (rad/sec)$','FontSize',16,'Interpreter','latex')
% legend({'$p$','$q$','$r$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% grid on;
% 
% 
% figure()
% subplot(2,1,1)
% hold on;
% plot(time(1:end-2),ut_graph(1:end-2), 'r','color','blue','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$u_t$(N)','FontSize',16,'Interpreter','latex')
% legend({'$u_{t}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% grid on;
% 
% hh = 1/25*(ones(1,5));
% fil_ur_graph = conv(hh,ur_graph);
% 
% subplot(2,1,2)
% hold on;
% plot(time(1:end-2),up_graph(1:end-2), 'r','color','green','linewidth',2);
% plot(time(1:end-2),uq_graph(1:end-2), 'r','color','red','linewidth',2);
% plot(time(1:end-2),fil_ur_graph(1:end-6), 'r','color','black','linewidth',2);cos(lambda)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$\tau$(N-m)','FontSize',16,'Interpreter','latex')
% legend({'$\tau_{p}$','$\tau_{q}$','$\tau_{r}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% grid on;

%% For animation uncomment the following section

% x = xq_graph(1:end-2);
% y = yq_graph(1:end-2);
% z = zq_graph(1:end-2);
% 
% xb = xb_graph(1:end-2);
% yb = yb_graph(1:end-2);
% zb = zb_graph(1:end-2);
% 
% phi = ph_graph(1:end-2);
% theta = th_graph(1:end-2);
% psi = ps_graph(1:end-2);
% 
% animatesimulation

%%
