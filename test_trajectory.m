%% Plotting desired plot
clc
clear all
close all

%%

des_radius = 1;
amp = 0.25;
freq = 10;
des_height = 10;

lambda = 0:0.01:2*pi;
ss = size(lambda)
z_lambda = zeros(1,ss(2)) + 1;
figure;



figure(1)
clf;
plot3(cos(lambda) ,sin(lambda) ,lambda*0 + 10 + amp*cos(freq*lambda), 'b', 'MarkerSize', 2, 'MarkerFaceColor', 'red');
grid on;



% xi1_1 = xb^2 - des_radius^2 + yb^2;
% xi2_1 = zb - des_height - amp*sin(freq*xb);